

function check(a, b) {
    if(a < b) return 'a<b';
    
    if(a > b) return 'a>b';
        
}

function doAction(a, b){
    if(a > 1000)  return 'a>1000';
    
    if(b < 1000) return 'b<1000';
    
    
}

module.exports = {
    check,
    doAction
} 